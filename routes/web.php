<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/', 'HomeController@home');

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');


Route::get('/', 'HomeController@home2');

Route::get('/data-table', 'HomeController@data_table');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/register', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome_get');

Route::post('/welcome', 'AuthController@welcome_post');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
