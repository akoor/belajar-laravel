<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name_user">First name:</label><br>
        <input type="text" name="first_name" id="first_name_user">
        <br /> <br>
        <label for="last_name_user">Last Name:</label><br>
        <input type="text" name="last_name" id="last_name_user">
        <br /><br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="Male"> Male <br>
        <input type="radio" name="gender" value="Female"> Female <br>
        <input type="radio" name="gender" value="Other"> Other <br>
        <br><br>
        <label>Nationality:</label> <br>
        <select>
            <option value="Indonesian">Indonesian </option>
            <option value="Singaporian">Singaporian </option>
            <option value="Malaysian">Malaysian </option>
            <option value="Australian">Australian </option>
        </select>
        <br><br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="language" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="English"> English <br>
        <input type="checkbox" name="language" value="Other"> Other <br><br>
        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>