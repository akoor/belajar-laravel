@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Create New Pertanyaan</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
        @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="judul">Judul Pertanyaan</label>
          <input type="text" class="form-control" name="judul" id="judul" value="{{old('judul', '')}}" placeholder="Judul Pertanyaan">
          @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <input type="text" class="form-control" name="isi" id="isi" value="{{old('isi', '')}}"placeholder="Isi Pertanyaan">
          @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection