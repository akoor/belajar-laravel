@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Pertanyaan {{$pertanyaans->id}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$pertanyaans->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="box-body">
        <div class="form-group">
          <label for="judul">judul</label>
          <input type="text" class="form-control" name="judul" id="judul" value="{{old('judul', $pertanyaans->judul)}}" placeholder="Isi Judul">
          @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <input type="text" class="form-control" name="isi" id="isi" value="{{old('isi', $pertanyaans->isi)}}"placeholder="isi">
          @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection