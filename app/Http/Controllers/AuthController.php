<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view("register");
    }

    public function welcome_get(Request $request){
        return view("welcome", ["nama" => $request["first_name"]]);
    }

    public function welcome_post(Request $request){
        // dd($request);
        $fname = $request['first_name'];
        $lname = $request['last_name'];
        // return view("welcome", ["nama" => $request["first_name"]]);
        return view('welcome', compact('fname', 'lname'));
    }
}
