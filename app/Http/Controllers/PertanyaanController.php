<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index(){
        // $pertanyaans = DB::table('pertanyaan')->get();
        $pertanyaans = Pertanyaan::all();

        // dd($pertanyaans);
        // $pertanyaans = Post::find();

        return view('pertanyaan.index', compact('pertanyaans'));
    }

    public function show($id){
        $pertanyaans = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($pertanyaans);

        return view('pertanyaan.show', compact('pertanyaans'));
    }
    
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        $query = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan/')->with('success', 'pertanyaan Berhasil Disimpan');
    }

    public function edit($id){
        $pertanyaans = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.edit', compact('pertanyaans'));
    }

    public function update($id, Request $request){
        // dd($request);
        $validatedData = $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required'
        ]);

        // $pertanyaans = DB::table('pertanyaan')
        //             ->where('id', $id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi' => $request['isi']
        //             ]);
        
        $pertanyaans = Pertanyaan::where('id', $id)->update([
                    "title" => $request["judul"],
                    "body" => $request["isi"]
        ]);

        return redirect('/pertanyaan/')->with('success', 'Berhasil update pertanyaan');
    }

    public function destroy($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan/')->with('success','pertanyaan berhasil di hapus!');
    }
    
}
